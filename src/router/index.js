import { createRouter, createWebHistory } from 'vue-router'

const HomeRoute = {
  path: '/',
  name: 'home',
  component: () => import('@/components/@Home')
}

// CASES

const GreenMarathonCaseRoute = {
  path: '/cases/green-marathon',
  name: 'green-marathon',
  props: true,
  component: () => import('@/components/@GreenMarathon'),
  meta: { case: true }
}

const HennessyCaseRoute = {
  path: '/cases/hennessy',
  name: 'hennessy',
  props: true,
  component: () => import('@/components/@Hennessy'),
  meta: { case: true }
}

const VolgaDneprCaseRoute = {
  path: '/cases/volga-dnepr',
  name: 'volga-dnepr',
  props: true,
  component: () => import('@/components/@VolgaDnepr'),
  meta: { case: true }
}

const routes = [
  HomeRoute,
  // cases
  GreenMarathonCaseRoute,
  HennessyCaseRoute,
  VolgaDneprCaseRoute
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,

  scrollBehavior(to, _, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else if (to.hash) {
      return {
        el: to.hash,
        behavior: 'smooth'
      }
    } else {
      return { top: 0 }
    }
  }
})

export default router
